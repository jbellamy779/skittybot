<div align="center">
    <p align="center">
        <a href="https://discord.gg/5s99Zpp">
            <img alt="SkittyBot" src="./app/assets/skittybot.png" width="600"></img>
        <a/>
    </p>
    <p align="center">
        <a href="https://discord.gg/5s99Zpp">
            <img src="https://img.shields.io/discord/412686231101505536.svg?style=flat-square&amp;label=The%20Cottage" />
        </a>
    <p/>
    <p align="center">
        A DiscordApp bot written on top of Discord.js
        <br />
        You can <a href="https://discordapp.com/oauth2/authorize?client_id=411787757455671296&amp;scope=bot">Invite</a> SkittyBot to your server.</span>
    </p>
</div>

# Installation
```bash
git clone https://gitlab.com/Skitty/skittybot.git
cd skittybot/
npm install
```

# Setup
This bot is plugin centric. All plugins are basically npm modules and if they are using any dependencies then you will need to `npm install` inside each plugin.
The bot will also concat all dependencies from plugins you added into the main `package.json`.
If you added something and ran the bot and it failed to load that plugin due to a missing dependency then you can shut it down and `npm install` again to install the missing dependency.
IF that didn't work then you'll have to `npm install` the dependency yourself.

Once that is done you can run the bot and configure the required settings like bot token, owner ID, default prefix and bot name over `http://localhost`.

To run the bot just open a CLI in its directory and do:
```bash
node .
```

Once started you should get some messages in your CLI which includes a link to its local web app. Go there, hit `sign in` top right, enter a username and password and hit `sign up`.
That creates your manager account, after that you can't create another account so make sure you don't forget those login details.

Once thats done configure required settings and hit save, the bot will start up and log in to its bot account and will be ready to go.

# Development
The bot will run in two modes, production and development.
Launching the bot normally causes it to use port `80` and port `443` if SSL is enabled.

If developing the bot set the environment variable `BOT_DEBUGGING` to `true` to run in development mode.
This causes the bot to use port `5080` and port `5443` for SSL instead to avoid collisions with another running in production.
In development mode the bot will create the folder `plugins-dev` under the current directory and attempt to load plugins from there instead of the normal plugins directory.