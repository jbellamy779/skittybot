'use strict';

let chalk = require('chalk');
chalk = new chalk.constructor({ level: 1 });

let ms = 0;

let r = chalk.redBright;
let g = chalk.greenBright;
let b = chalk.blueBright;
let y = chalk.yellowBright;
let m = chalk.magentaBright;

let enable_debug = 0;
let enable_info = 0;

class Logger {
    constructor() {
        this.console = global.console;
        switch (process.env.BOT_DEBUGGING) {
            case 'true':

                ms = 1;
                enable_debug = 2;
                enable_info = 1;

                break;
            default:
                enable_info = 1;
        }
    }

    time() {
        let time = new Date();
        let date = time.toLocaleString('en-US', { month: 'short', day: 'numeric', year: 'numeric' });
        let hour = time.getHours();
        let period = hour >= 12 ? 'PM' : 'AM';
        hour = String(hour % 12 || 12).padStart(2, '0');
        let min = String(time.getMinutes()).padStart(2, '0');
        let sec = String(time.getSeconds()).padStart(2, '0');
        let str = `${date} ${hour}:${min}:${sec} ${period}`;
        return str;
    }

    timems() {
        let time = new Date();
        let date = time.toLocaleString('en-US', { month: 'short', day: 'numeric', year: 'numeric' });
        let hour = time.getHours();
        let period = hour >= 12 ? 'PM' : 'AM';
        hour = String(hour % 12 || 12).padStart(2, '0');
        let min = String(time.getMinutes()).padStart(2, '0');
        let sec = String(time.getSeconds()).padStart(2, '0');
        let mic = String(time.getMilliseconds()).padStart(3, '0');
        let str = `${date} ${hour}:${min}:${sec}.${mic} ${period}`;
        return str;
    }

    error(...a) {
        let ts = `[ERROR : ${ms ? this.timems() : this.time()}]`;
        this.collect(ts, ...a);
        this.console.log(r(ts), ...a);
    }

    warn(...a) {
        this.collect(`[WARN  : ${ms ? this.timems() : this.time()}]`, ...a);
        this.console.log(y(`[WARN  : ${ms ? this.timems() : this.time()}]`), ...a);
    }

    info(...a) {
        if (this.collector) {
            this.collect(`[INFO  : ${ms ? this.timems() : this.time()}]`, ...a);
        }
        if (enable_info) this.console.log(b(`[INFO  : ${ms ? this.timems() : this.time()}]`), ...a);
    }

    log(...a) {
        let ts = `[LOG   : ${ms ? this.timems() : this.time()}]`;
        this.collect(ts, ...a);
        this.console.log(g(ts), ...a);
    }

    debug(...a) {
        if (this.collector) {
            this.collect(`[DEBUG : ${ms ? this.timems() : this.time()}]`, ...a);
        }
        if (enable_debug) this.console.log(b(`[DEBUG : ${ms ? this.timems() : this.time()}]`), ...a);
    }

    verbose(...a) {
        if (enable_debug > 1) {
            let ts = `[EXTDBG: ${ms ? this.timems() : this.time()}]`;
            this.collect(ts, ...a);
            this.console.log(m(ts), ...a);
        } else if (this.collector) {
            let ts = `[EXTDBG: ${ms ? this.timems() : this.time()}]`;
            this.collect(ts, ...a);
        }
    }

    reportError(error) {
        let ref, stack;
        stack = (ref = error.stack) !== null ? ref : `${error.name}: ${error.message}`;
        let str = `An error was encountered which requires your attention:\n${stack}`;
        this.collect(str);
        this.console.error(str);
    }

    log_command(...a) {
        this.console.log(y(`[CMD   : ${ms ? this.timems() : this.time()}]`), ...a);
    }

    collect(...args) {
        // small hack to allow collecting formatted log output
        if (this.collector && this.collector instanceof Function) this.collector(...args);
    }

    setCaptureFunction(func) {
        if (func instanceof Function) {
            this.collector = func;
        } else {
            throw new Error(`func not instance of Function!`);
        }
    }

    stopCapture() {
        delete this.collector;
    }

}

module.exports = new Logger();
