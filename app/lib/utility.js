'use strict';

const { URL } = require('url');
const fetch = require('node-fetch');
const fileType = require('file-type');
const sizeOf = require('image-size');
const crypto = require('crypto');
const fs = require('fs');
const path = require('path');

const compendium = require('./compendium.js');

const domain_extension_blacklist = {
    mil: {
        com: `Military domains scare me.. What if they don't want me touching anything there? I'd rather not, sorry..`,
        allow: false
    },
    army: {
        com: `Army domains scare me.. What if they don't want me touching anything there? I'd rather not, sorry..`,
        allow: false
    },
    navy: {
        com: `Navy domains scare me.. What if they don't want me touching anything there? I'd rather not, sorry..`,
        allow: false
    },
    gov: {
        com: `Keep your government boogeyman URLs OFF OF ME!! NO! But really, what if they don't want me touching something there? I'd just rather not, sorry...`,
        allow: false
    },
    arpa: {
        com: `These arpa domains never work for me.`,
        allow: false
    },
    airforce: {
        com: `That domain scares me a little, I'd rather not be making requests to a place like that...`,
        allow: false
    },
    tk: {
        com: `Those .tk domains are pretty terrible, I'd be careful with those if I were you.`,
        allow: true
    }
};

class image_fetcher {
    constructor(options = {}) {
        // Max bytes
        this.maxDownSize = options.fileSize ? options.fileSize : 8e+6;

        this.maxDimension = options.dimension ? options.dimension : 10000;

        this.mime_filter = options.mimes ? options.mimes : {
            png: true,
            jpg: true,
            jpeg: true
        };
    }

    async requestImage(url) {
        let res = await fetch(url, { method: 'HEAD' }).catch(console.error);
        // don't even bother if the server cannot support head requests
        if (!res) return false;
        let headers = res.headers;
        if (!headers) return false;
        let mime_check = headers.get('content-type');
        if (mime_check) {
            mime_check = mime_check.split('/');
            if (Array.isArray(mime_check)) {
                if (mime_check.length > 1) {
                    if (!this.mime_filter[mime_check[1]]) return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

        let downsize = parseInt(headers.get('content-length'));
        if (downsize > this.maxDownSize) {
            // msg.channel.send([
            //     `Server reports image is ${(downsize / 1e+6).toFixed(2)} MB AND THATS TOO BIG!`,
            //     `Anything under ${Math.floor(this.maxDownSize / 1e+6)} MB is OK.`
            // ]);
            return false;
        }

        let image_data = await fetch(url).catch(console.error);

        if (!image_data) return false;
        let buff = await image_data.buffer();
        let type = await fileType(buff);

        if (!type.ext) return false;

        if (!this.mime_filter[type.ext]) return false;

        let dimensions = await sizeOf(buff);
        if (!dimensions) return false;

        if (dimensions.type !== type.ext) return false;

        if (dimensions.height > this.maxDimension || dimensions.width > this.maxDimension) return false;

        return {
            url: url,
            body: buff,
            h: dimensions.height,
            w: dimensions.width,
            type: type.ext
        };
    }

    async requestFile(url) {
        let info = await fetch(url, { method: 'HEAD' }).catch(console.error);
        // don't even bother if the server cannot support head requests
        if (!info && !info.headers) return false;
        let headers = info.headers;
        let mime_check = headers.get('content-type');
        if (mime_check) {
            mime_check = mime_check.split('/');
            if (Array.isArray(mime_check)) {
                if (mime_check.length > 1) {
                    if (!this.mime_filter[mime_check[1]]) return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

        let downsize = parseInt(headers['content-length']);
        if (downsize > this.maxDownSize) {
            // msg.channel.send([
            //     `Server reports image is ${(downsize / 1e+6).toFixed(2)} MB AND THATS TOO BIG!`,
            //     `Anything under ${Math.floor(this.maxDownSize / 1e+6)} MB is OK.`
            // ]);
            return false;
        }

        let file_Data = await fetch(url).catch(console.error);

        if (!file_Data.body) return false;
        let body = await file_Data.body.buffer();

        if (!body) return false;

        return {
            body: body
        };
    }

    isLink(url) {
        if (!/^https?:\/\//.test(url)) url = `https://${url}`;
        let aURL;
        try {
            aURL = new URL(url);
        } catch (e) {
            return false;
        }
        if (!/^https?:/.test(aURL.protocol)) return false;
        let host = aURL.host.split('.');
        if (host.length < 2) return false;
        let domain_extension = host[host.length - 1];
        if (domain_extension.length < 1) return false;
        let blc = domain_extension_blacklist[domain_extension.toLowerCase()];
        if (blc) return false;
        return aURL.href;
    }

    getActualLink(embed) {
        if (embed.provider && embed.provider.name) {
            let provider = embed.provider.name.toLowerCase();
            if (provider === 'imgur') {
                let aURL = embed.thumbnail.url.split('.');
                embed.url += `.${aURL[aURL.length - 1]}`;
            }
        }
        return embed.url;
    }

    async findFirstFile(msg, args) {
        let file_link;
        let file_blob;
        if (args && args[0]) {
            for (let i = 0; i < args.length; i++) {
                if (isNaN(args[i])) {
                    file_link = this.isLink(args[i]);
                    if (file_link) break;
                }
            }
        }
        if (!file_link) {
            // find first attachment
            let key = msg.attachments.keys().next().value;
            if (key) {
                let attachment = msg.attachments.get(key);
                if (attachment) file_link = attachment.proxyURL;
            }
        }
        if (!file_link) {
            await msg.channel.messages.fetch({ limit: 30 })
                .then(async messages => {
                    let attachment_key;
                    let current;
                    var msgSet = new Set(messages.keys());
                    for (let key of Array.from(msgSet)) {
                        current = messages.get(key);
                        attachment_key = current.attachments.keys().next().value;
                        if (attachment_key) {
                            let attachment = current.attachments.get(attachment_key);
                            if (attachment) {
                                if (attachment.url) {
                                    file_blob = await this.requestFile(attachment.url);
                                    if (file_blob) break;
                                }
                            }
                        }
                    }
                })
                .catch(console.error);
        }
        if (file_link && !file_blob) file_blob = await this.requestImage(file_link);
        if (file_blob) return file_blob;
        return false;
    }

    async findFirst(msg, args) {
        let img_link;
        let image_object;
        if (args && args[0]) {
            for (let i = 0; i < args.length; i++) {
                if (isNaN(args[i])) {
                    img_link = this.isLink(args[i]);
                    if (img_link) break;
                }
            }

            // TODO: search through argument tokens for a mention instead
            let user = msg.mentions.users.last();
            if (user && user.id !== SkittyBot.client.id) {
                let av = user.displayAvatarURL({
                    format: 'png',
                    size: 2048
                });
                if (av) img_link = av;
            }

        }
        if (!img_link) {
            // find first attachment
            let key = msg.attachments.keys().next().value;
            if (key) {
                let attachment = msg.attachments.get(key);
                if (attachment) img_link = attachment.proxyURL;
            }
        }
        if (!img_link) {
            await msg.channel.messages.fetch({ limit: 30 })
                .then(async messages => {
                    let attachment_key;
                    let current;
                    var msgSet = new Set(messages.keys());
                    for (let key of Array.from(msgSet)) {
                        current = messages.get(key);
                        attachment_key = current.attachments.keys().next().value;
                        if (attachment_key) {
                            let attachment = current.attachments.get(attachment_key);
                            if (attachment) {
                                if (attachment.proxyURL) {
                                    image_object = await this.requestImage(attachment.proxyURL);
                                    if (image_object) break;
                                }
                            }
                        }
                        if (Array.isArray(current.embeds) && current.embeds.length >= 1) {
                            let embed = current.embeds[0];
                            if (embed.type === 'image' && embed.url) {
                                if (embed.provider) embed.url = this.getActualLink(embed);
                                img_link = this.isLink(embed.url);
                                if (img_link) {
                                    image_object = await this.requestImage(img_link);
                                    img_link = null;
                                    if (image_object) break;
                                }
                            } else if (embed.type === 'article' && embed.thumbnail) {
                                img_link = this.isLink(embed.thumbnail.proxyURL);
                                if (img_link) {
                                    image_object = await this.requestImage(img_link);
                                    img_link = null;
                                    if (image_object) break;
                                }
                            } else if (embed.image && embed.image.url) {
                                img_link = this.isLink(embed.image.url);
                                if (img_link) {
                                    image_object = await this.requestImage(img_link);
                                    img_link = null;
                                    if (image_object) break;
                                }
                            } else if (embed.thumbnail && embed.thumbnail.url) {
                                img_link = this.isLink(embed.thumbnail.url);
                                if (img_link) {
                                    image_object = await this.requestImage(img_link);
                                    img_link = null;
                                    if (image_object) break;
                                }
                            }
                        }
                    }
                })
                .catch(console.error);
        }
        if (img_link && !image_object) image_object = await this.requestImage(img_link);
        if (image_object) return image_object;
        return false;
    }
}

module.exports.image_fetcher = image_fetcher;

// https://discordapp.com/developers/docs/resources/channel#embed-limits

module.exports.trunc = class trunc {
    static clip(txt, at) {
        return txt.length > at ? `${txt.substr(0, at - 1)}…` : txt;
    }
    static title(txt) {
        return this.clip(txt, 256);
    }
    static fieldName(txt) {
        return this.title(txt);
    }
    static fieldValue(txt) {
        return this.clip(txt, 1024);
    }
    static description(txt) {
        return this.clip(txt, 2048);
    }
};

module.exports.capitalizeFirst = string => string[0].toUpperCase() + string.slice(1);

module.exports.cleanForJSON = text => {
    // JavaScript supports all of JSON except for these 2 characters which will cause syntax errors
    // Most JSON parser libraries should handle this automatically, but JSON.parse does not
    // Line separator (U+2028)
    let step1 = String(text).replace(/\u2028/g, '');
    // Paragraph separator (U+2029)
    return step1.replace(/\u2029/g, '');
};

module.exports.uptime = () => {
    let seconds = process.uptime();
    let days = Math.floor(seconds / (3600 * 24));
    seconds -= days * 3600 * 24;
    let hours = Math.floor(seconds / 3600);
    seconds -= hours * 3600;
    let minutes = Math.floor(seconds / 60);
    seconds -= minutes * 60;
    seconds = Math.floor(seconds);

    seconds = String(seconds).padStart(2, '0');
    minutes = String(minutes).padStart(2, '0');
    hours = String(hours).padStart(2, '0');
    days = String(days).padStart(2, '0');

    return `${days}:${hours}:${minutes}:${seconds}`;
};

module.exports.fetch_directories = srcpath => fs.readdirSync(srcpath).filter(file => fs.statSync(path.join(srcpath, file)).isDirectory());

module.exports.fetch_assets = srcpath => {
    if (!fs.existsSync(srcpath)) return [];
    return fs.readdirSync(srcpath).filter(file => fs.statSync(path.join(srcpath, file)).isFile());
};

module.exports.read = file => new Promise((resolve, reject) => {
    if (fs.existsSync(file)) {
        fs.readFile(file, (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data.toString());
        });
    }
});

module.exports.getCaller = () => {
    // Save original Error.prepareStackTrace
    var origPrepareStackTrace = Error.prepareStackTrace;

    // Override with function that just returns `stack`
    Error.prepareStackTrace = (_, stack) => stack;

    // Create a new `Error`, which automatically gets `stack`
    var err = new Error();

    // Evaluate `err.stack`, which calls our new `Error.prepareStackTrace`
    var stack = err.stack;

    // Restore original `Error.prepareStackTrace`
    Error.prepareStackTrace = origPrepareStackTrace;

    // Remove superfluous function call on stack
    // getStack --> Error
    stack.shift();

    return path.dirname(stack[1].getFileName());
};

module.exports.open = url => {
    let spawn = require('child_process').spawn;
    let commands = {
        darwin: p => spawn('open', [p]),
        win32: p => spawn('cmd', ['/c', 'start', p]),
        default: p => spawn('xdg-open', [p])
    };
    let cmd = commands[process.platform] || commands.default;
    var child = cmd(url);
    var errorText = '';
    child.stderr.setEncoding('utf8');
    child.stderr.on('data', data => {
        errorText += data;
    });
    child.stderr.on('end', () => {
        if (errorText.length > 0) {
            console.error(new Error(errorText));
        }
    });
};

module.exports.prompt = msg => new Promise((resolve, reject) => {
    process.stdout.write(msg);
    process.stdin.resume();
    process.stdin.setEncoding('utf8');
    process.stdin.on('data', text => {
        text = text.replace(/\r|\n/g, '');
        if (/\s/.test(text) || text.length < 6) {
            reject(new Error(`Not long enough. Try again.`));
            return;
        }
        resolve(text);
    });
});

module.exports.genID = () => ([1e3] + -1e3 + -4e3 + -8e3).replace(/[018]/g, a => ((a ^ Math.random() * 16) >> a / 4).toString(16));

module.exports.md5 = blob => crypto.createHash('md5').update(blob).digest('hex');

/**
 * Retrieves a paramter
 * @param {string} param paramter to find
 * @param {string} text the text to search in
 * @param {string} defaultValue a default value to return when a paramter value was not found
 * @returns {string|null} null if no value, string paramter if found.
 */
module.exports.param = (param, text, defaultValue = null) => {
    let m = text.match(new RegExp(`--${param}\\s{1,}([\\s\\S]*?)(?:\\s--|\\s?$)`, 'i'));
    if (m) return m.pop();
    m = text.match(new RegExp(`--${param}\\s{0,1}$`, 'i'));
    if (m) return true;
    return defaultValue;
};

/**
 * validates a URL
 * @param {string} url the url to check
 * @returns {boolean} true if valid false if something isn't right
 */
module.exports.isValidUrl = url => {
    // note: should use the list of valid domains
    let pattern = `^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$`;
    let regexQuery = new RegExp(pattern, 'i');
    return !!regexQuery.test(url);
};

/**
 * Extracts as much human readable text content from a discord message as possible
 * @param {string} message message object to search through
 * @param {string} encapsulateEmbed whether or not to return the value enclosed in embed tags
 * @returns {string} all the text that was found in a message, treeified
 */
module.exports.textify = (message, encapsulateEmbed = false) => {
    let tags = '```';
    if (Array.isArray(message.embeds) && message.embeds.length < 1) {
        return message.cleanContent;
    }
    let e = message.embeds[0];
    let embed = {};
    if (message.cleanContent) embed.MESSAGE = message.cleanContent;
    if (e.title) embed.TITLE = e.title.replace(tags, `'''`);
    if (e.type) embed.TYPE = e.type;
    if (e.author) {
        embed.AUTHOR = {
            NAME: e.author.name.replace(tags, `'''`),
            URL: e.author.url.replace(tags, `'''`),
        };
    }
    if (e.description) embed.DESC = e.description.replace(tags, `'''`);
    if (typeof e.fields === 'object' && Array.isArray(e.fields)) {
        e.fields.forEach(o => {
            embed[`FIELD: ${o.name.replace(tags, `'''`)}`] = {
                VALUE: o.value.replace(tags, `'''`)
            };
        });
    }
    if (e.url) embed.URL = e.url;
    if (e.image && e.image.hasOwnProperty('url')) {
        embed.IMAGE = e.image.url;
        embed.IMAGE_PROXY = e.image.proxyURL;
    }
    if (e.video && e.video.hasOwnProperty('url')) {
        embed.VIDEO = e.video.url;
    }
    if (e.provider && e.provider.hasOwnProperty('url')) {
        if (e.provider.name) embed.PROVIDER = e.provider.name;
        embed.PROVIDER = e.provider.url;
    }
    if (e.thumbnail && e.thumbnail.hasOwnProperty('url')) {
        embed.THUMBNAIL = e.thumbnail.url;
    }
    if (e.footer && e.footer.hasOwnProperty('text')) {
        embed.FOOTER = e.footer.text.replace(tags, `'''`);
    }
    let txt = SkittyBot.utils.treeify({ EMBED: embed }).join('\n');
    return encapsulateEmbed ? `${tags}\n${txt}${tags}` : txt;
};

class treeify {

    static strip(s) {
        return `${s}`.replace(/[^\S\r\n]/g, String.fromCharCode(160));
    }

    static gen(key, root, type, last, lastStates, cb) {
        let line = '',
            index = 0,
            lastKey,
            lastStatesCopy = Array.from(lastStates),
            nbsp = String.fromCharCode(160);
        if (lastStatesCopy.push([root, last]) && lastStates.length > 0) {
            lastStates.forEach((lastState, idx) => {
                if (idx > 1) {
                    line += (lastState[1] ? nbsp : '│') + nbsp + nbsp;
                }
            });

            if (lastStates.length > 1) {
                let str = last ? '└' : '├';
                if (key) str += '─ ';

                if (type instanceof Array) {
                    let tokens = this.strip(root).split(nbsp);
                    let n = 0, sentences = [];
                    tokens.forEach((word, i) => {
                        if (!((i + 1) % 15)) {
                            ++n;
                        }
                        if (!sentences[n]) sentences[n] = [];
                        sentences[n].push(`${word}`);
                    });
                    line = sentences.map((a, i) => `${line}${i === sentences.length - 1 ? str : '├─ '}${a.join(' ')}`);
                } else {
                    line += str + this.strip(key);
                }
            } else {
                line += this.strip(key);
            }
            if (typeof root !== 'object') {
                if (type instanceof Array) {
                    // line += this.strip(root);
                } else {
                    line += `:${nbsp + this.strip(root)}`;
                }
            } else {
                line += '/';
            }
            // eslint-disable-next-line callback-return
            cb(line);
        }
        if (root instanceof Object) {
            let keys = Object.keys(root);
            keys.forEach(branch => {
                lastKey = ++index === keys.length;
                this.gen(branch, root[branch], root, lastKey, lastStatesCopy, cb);
            });
        }
    }

    static create(obj) {
        let tree = [];
        this.gen('.', obj, obj, false, [], line => {
            if (typeof line === 'object') {
                tree.push(...line);
            } else {
                tree.push(line);
            }
        });
        return tree;
    }

}

module.exports.treeify = treeify.create.bind(treeify);

// sql-escape-string: taken from https://github.com/thlorenz/sql-escape-string/blob/master/sql-escape-string.js
// eslint-disable-next-line no-control-regex
const CHARS_GLOBAL_BACKSLASH_SUPPORTED_RX = /[\0\b\t\n\r\x1a"'\\]/g;
const CHARS_ESCAPE_BACKSLASH_SUPPORTED_MAP = {
    '\0': '\\0',
    '\b': '\\b',
    '\t': '\\t',
    '\n': '\\n',
    '\r': '\\r',
    '\x1a': '\\Z',
    '"': '\\"',
    '\'': '\\\'',
    '\\': '\\\\'
};

module.exports.escapeString = (val, opts) => {
    if (val === null) {
        throw new Error('Need to pass a valid string');
    }
    val = `${val}`;
    opts = opts || {};
    const backslashSupported = !!opts.backslashSupported;

    if (!backslashSupported) return `'${val.replace(/'/g, "''")}'`;

    const charsRx = CHARS_GLOBAL_BACKSLASH_SUPPORTED_RX;
    const charsEscapeMap = CHARS_ESCAPE_BACKSLASH_SUPPORTED_MAP;
    var chunkIndex = charsRx.lastIndex = 0;
    var escapedVal = '';
    var match;

    // eslint-disable-next-line no-cond-assign
    while (match = charsRx.exec(val)) {
        escapedVal += val.slice(chunkIndex, match.index) + charsEscapeMap[match[0]];
        chunkIndex = charsRx.lastIndex;
    }

    // Nothing was escaped
    if (chunkIndex === 0) return `'${val}'`;

    if (chunkIndex < val.length) return `${escapedVal}${val.slice(chunkIndex)}`;
    return `${escapedVal}`;
};

module.exports.compendium = compendium;
