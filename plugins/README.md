# SkittyBot plugins
The bot will always look for three folders inside this folder from which it will attempt to load plugins with commands.
The bots default commands are found in the `default` folder.

The bot will load plugins and create commands from these three folders in the below order:
1. default
2. private
3. public

Commands created from `private` plugins cannot override commands created in `default` plugins and `public` cannot override commands created from `private` and `default` plugins.

# Why public and private?
There's really no difference between these two folders other than allowing you to choose where to drop your plugins based on their target audience.
For example, if you're going to create or use a plugin which only you can use as the bot owner then you'd want to add it to the `private` plugin folder.
For plugins that all users of the bot can use, you'd want to put in in the `public` plugins directory.

This folder structure only exists to help you differentiate between what only you can use and what anyone else can use.

# How do I create my own plugin?
The `public` plugins directory contains an example plugin that demonstrates simple usage of the global `SkittyBot` object which handles all user input and command usage.
You will need prior experience with `npm modules`, `discord.js` using the `master` branch, and scripting with `node.js` to help you get something done.