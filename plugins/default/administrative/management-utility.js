'use strict';

// src: https://github.com/discordjs/Commando/blob/7aa08a6109b59e8794e9b4aa8364e4a189d3a4f7/src/util.js#L19
const permissions = {
    ADMINISTRATOR: 'Administrator',
    VIEW_AUDIT_LOG: 'View audit log',
    MANAGE_GUILD: 'Manage server',
    MANAGE_ROLES: 'Manage roles',
    MANAGE_CHANNELS: 'Manage channels',
    KICK_MEMBERS: 'Kick members',
    BAN_MEMBERS: 'Ban members',
    CREATE_INSTANT_INVITE: 'Create instant invite',
    CHANGE_NICKNAME: 'Change nickname',
    MANAGE_NICKNAMES: 'Manage nicknames',
    MANAGE_EMOJIS: 'Manage emojis',
    MANAGE_WEBHOOKS: 'Manage webhooks',
    VIEW_CHANNEL: 'Read text channels and see voice channels',
    SEND_MESSAGES: 'Send messages',
    SEND_TTS_MESSAGES: 'Send TTS messages',
    MANAGE_MESSAGES: 'Manage messages',
    EMBED_LINKS: 'Embed links',
    ATTACH_FILES: 'Attach files',
    READ_MESSAGE_HISTORY: 'Read message history',
    MENTION_EVERYONE: 'Mention everyone',
    USE_EXTERNAL_EMOJIS: 'Use external emojis',
    ADD_REACTIONS: 'Add reactions',
    CONNECT: 'Connect',
    SPEAK: 'Speak',
    MUTE_MEMBERS: 'Mute members',
    DEAFEN_MEMBERS: 'Deafen members',
    MOVE_MEMBERS: 'Move members',
    USE_VAD: 'Use voice activity'
};

const { MessageEmbed } = require('discord.js');
const path = require('path');

const { param } = require(path.join(SkittyBot.config.location.lib, 'utility.js'));

var m_cap = null;

class message_capture {
    constructor() {
        this.max_messages = 25;
        this.deleted = new Map();
        SkittyBot.client.on(SkittyBot.event.MESSAGE_DELETE, this.MESSAGE_DELETE.bind(this));
    }
    MESSAGE_DELETE(msg) {
        if (msg.content.length < 2) return;
        let cache = this.deleted.get(msg.author.id);
        if (cache !== undefined) {
            cache.unshift(msg);
            if (cache.length > this.max_messages) cache.pop();
        } else {
            cache = [msg];
        }
        this.deleted.set(msg.author.id, cache);
    }
    destroy() {
        SkittyBot.client.removeListener(SkittyBot.event.MESSAGE_DELETE, this.MESSAGE_DELETE.bind(this));
    }
}

class blacklist_handler {

    static isAdmin(msg) {
        return SkittyBot.administrators.has(msg.author.id);
    }

    static canDisableModule(msg, name) {
        let target = name.toLowerCase();
        let mod;
        SkittyBot.plugins.list.forEach(p => {
            if (!p.private && p.plugin_title.toLowerCase() === target) mod = p;
        });
        if (mod) {
            if (mod.noDisable) {
                msg.reply(`This module cannot be disabled.`);
                return false;
            }
        } else if (!mod) {
            msg.reply(`Looks like whatever "${name}" is doesn't match any of my module names. Check your input and try again.`);
            return false;
        }
        return mod.plugin_title ? mod.plugin_title.toLowerCase() : 'undefined';
    }

    static canDisableCommand(msg, name) {
        let target = name.toLowerCase();
        let cmd = false;
        let noDisable = false;
        SkittyBot.plugins.list.forEach(p => {
            if (cmd) return;
            if (!p.private && p.enabled) {
                p.commands.forEach(c => {
                    if (cmd) return;
                    if (!c.private) {
                        if (c.command.toLowerCase() === target) {
                            if (c.noDisable) noDisable = true;
                            cmd = target;
                        }
                        if (Array.isArray(c.aliases)) {
                            c.aliases.forEach(a => {
                                if (a.toLowerCase() === target) {
                                    if (c.noDisable) noDisable = true;
                                    cmd = c.command.toLowerCase();
                                }
                            });
                        }
                    }
                });
            }
        });
        if (cmd) {
            if (noDisable) {
                msg.reply(`This command cannot be disabled.`);
                return false;
            }
        }
        return cmd;
    }

    static async block_module(msg, args, channel, block, isglobal) {
        if (isglobal && !this.isAdmin(msg)) return;
        isglobal = isglobal ? 1 : 0;
        let plugin_title = args.join(' ');
        if (!plugin_title) {
            if (!isglobal) msg.reply(`Pass this command name to the \`help\` command to view detailed information on how to use this command.`);
            return;
        }
        let plugin = this.canDisableModule(msg, plugin_title);
        if (plugin) {
            let channel_id = 0;
            let guild_id = msg.guild.id;
            if (channel) channel_id = msg.channel.id;
            if (isglobal) guild_id = 0;
            let res = await SkittyBot.system_database.get(`SELECT * FROM blacklist_modules WHERE guild_id = ? AND channel_id = ? AND module = ? AND isglobal = ?;`, guild_id, channel_id, plugin, isglobal);
            if (block && res && res.module) {
                if (!isglobal) msg.reply(`This module was already disabled in this ${channel ? `channel` : `guild`}.`);
                return;
            }
            if (!block && !res) {
                if (!isglobal) msg.reply(`This module was not previously disabled in this ${channel ? `channel` : `guild`}.`);
                return;
            }
            if (block) {
                SkittyBot.system_database.run('INSERT INTO blacklist_modules (guild_id, channel_id, module, isglobal) VALUES (? ,?, ?, ?);', guild_id, channel_id, plugin, isglobal)
                    .then(() => {
                        if (isglobal) {
                            SkittyBot.processor.cache_global_blacklist = false;
                        } else {
                            // TODO: Fix this, try not to re-query every for all guilds and delete only relevant data
                            SkittyBot.channel_module_blacklist.clear();
                            SkittyBot.guild_module_blacklist.clear();
                            SkittyBot.processor.cache_guild_module_blacklist.clear();
                            msg.reply(`Ok, I won't let anyone use commands from this module in this ${channel ? `channel` : `guild`} anymore.`);
                        }
                    })
                    .catch(e => {
                        console.error(e);
                        msg.reply(`Looks like something went wrong while trying to do that... Sorry.`);
                    });
            } else {
                res = await SkittyBot.system_database.run(`DELETE FROM blacklist_modules WHERE guild_id = ? AND channel_id = ? AND module = ? AND isglobal = ?;`, guild_id, channel_id, plugin, isglobal);
                if (res.changes) {
                    if (isglobal) {
                        SkittyBot.processor.cache_global_blacklist = false;
                    } else {
                        SkittyBot.channel_module_blacklist.clear();
                        SkittyBot.guild_module_blacklist.clear();
                        SkittyBot.processor.cache_guild_module_blacklist.clear();
                        msg.reply(`Ok, people will be able to use everything in ${plugin_title} here again.`);
                    }
                } else if (!isglobal) {
                    msg.reply(`Looks like something went wrong while trying to do that... Sorry.`);
                }
            }
        }
    }

    static async block_command(msg, args, channel, block, isglobal) {
        if (isglobal && !this.isAdmin(msg)) return;
        isglobal = isglobal ? 1 : 0;
        let command = args.join(' ');
        if (!command) {
            if (!isglobal) msg.reply(`Pass this command name to the \`help\` command to view detailed information on how to use this command.`);
            return;
        }
        command = this.canDisableCommand(msg, command);
        if (command) {
            let channel_id = 0;
            let guild_id = msg.guild.id;
            if (channel) channel_id = msg.channel.id;
            if (isglobal) guild_id = 0;
            let res = await SkittyBot.system_database.get(`SELECT * FROM blacklist_commands WHERE guild_id = ? AND channel_id = ? AND command = ? AND isglobal = ?;`, guild_id, channel_id, command, isglobal);
            if (block && res && res.command) {
                if (!isglobal) msg.reply(`This command was already disabled in this ${channel ? `channel` : `guild`}.`);
                return;
            }
            if (!block && !res) {
                if (!isglobal) msg.reply(`This command was not previously disabled in this ${channel ? `channel` : `guild`}.`);
                return;
            }
            if (block) {
                SkittyBot.system_database.run('INSERT INTO blacklist_commands (guild_id, channel_id, command, isglobal) VALUES (? ,?, ?, ?);', guild_id, channel_id, command, isglobal)
                    .then(() => {
                        if (isglobal) {
                            SkittyBot.processor.cache_global_blacklist = false;
                        } else {
                            SkittyBot.channel_command_blacklist.clear();
                            SkittyBot.guild_command_blacklist.clear();
                            SkittyBot.processor.cache_guild_commands_blacklist.clear();
                            msg.reply(`Ok, I won't let anyone use this command in this ${channel ? `channel` : `guild`} anymore.`);
                        }
                    })
                    .catch(e => {
                        console.error(e);
                        msg.reply(`Looks like something went wrong while trying to do that... Sorry.`);
                    });
            } else {
                res = await SkittyBot.system_database.run(`DELETE FROM blacklist_commands WHERE guild_id = ? AND channel_id = ? AND command = ? AND isglobal = ?;`, guild_id, channel_id, command, isglobal);
                if (res.changes) {
                    if (isglobal) {
                        SkittyBot.processor.cache_global_blacklist = false;
                    } else {
                        SkittyBot.channel_command_blacklist.clear();
                        SkittyBot.guild_command_blacklist.clear();
                        SkittyBot.processor.cache_guild_commands_blacklist.clear();
                        msg.reply(`Ok, people will be able to use ${command} here again.`);
                    }
                } else if (!isglobal) {
                    msg.reply(`Looks like something went wrong while trying to do that... Sorry.`);
                }
            }
        }
    }

    static async block_user(msg, args, channel, block, isglobal) {
        if (isglobal && !this.isAdmin(msg)) return;
        isglobal = isglobal ? 1 : 0;
        let user_id;
        let user = msg.mentions.users.last();
        if (user) user_id = user.id;
        if (!user_id && !isNaN(args[0])) user_id = args[0];
        if (!user_id) {
            msg.reply(`To block a user, mention them with this command or use their ID.`);
            return;
        }
        if (msg.author.id === user_id) {
            msg.reply(`You can't block yourself.`);
            return;
        }
        let channel_id = 0;
        let guild_id = msg.guild.id;
        if (channel) channel_id = msg.channel.id;
        if (isglobal) guild_id = 0;
        let res = await SkittyBot.system_database.get(`SELECT * FROM blacklist_user WHERE guild_id = ? AND channel_id = ? AND user_id = ? AND isglobal = ?;`, guild_id, channel_id, user_id, isglobal);
        if (block && res && res.user_id) {
            if (isglobal) {
                msg.reply(`I was already ignoring them.`);
            } else {
                msg.reply(`I was already ignoring them in this ${channel ? `channel` : `guild`}.`);
            }
            return;
        }
        if (!block && !res) {
            if (isglobal) {
                msg.reply(`I wasn't ignoring them.`);
            } else {
                msg.reply(`I wasn't ignoring them in this ${channel ? `channel` : `guild`}.`);
            }
            return;
        }
        if (block) {
            SkittyBot.system_database.run('INSERT INTO blacklist_user (guild_id, channel_id, user_id, isglobal) VALUES (? ,?, ?, ?);', guild_id, channel_id, user_id, isglobal)
                .then(() => {
                    if (isglobal) {
                        msg.reply(`Ok, I've added that user to my global ignore list.`);
                    } else {
                        msg.reply(`Ok, I'll ignore them in this ${channel ? `channel` : `guild`} from now on.`);
                    }
                    SkittyBot.processor.cache_user_blacklist = false;
                })
                .catch(e => {
                    console.error(e);
                    msg.reply(`Looks like something went wrong while trying to do that... Sorry.`);
                });
        } else {
            res = await SkittyBot.system_database.run(`DELETE FROM blacklist_user WHERE guild_id = ? AND channel_id = ? AND user_id = ? AND isglobal = ?;`, guild_id, channel_id, user_id, isglobal);
            if (res.changes) {
                if (isglobal) {
                    msg.reply(`Ok, I wont ignore them anymore.`);
                } else {
                    msg.reply(`Ok, I wont ignore them in this ${channel ? `channel` : `guild`} anymore.`);
                }
                SkittyBot.processor.cache_user_blacklist = false;
            } else if (!isglobal) {
                msg.reply(`Looks like something went wrong while trying to do that... Sorry.`);
            }
        }
    }
}

function bot_list_blocked_items(msg, args) {
    let guild_id;
    if (SkittyBot.config.owner_id === msg.author.id && !isNaN(args[0])) guild_id = args[0];
    if (!guild_id) guild_id = msg.guild.id;
    let message = [];
    let commands = [];
    let modules = [];
    let tag = '```';

    let bl = SkittyBot.guild_command_blacklist.get(msg.guild.id);
    if (Array.isArray(bl)) bl.forEach(t => commands.push(t));
    if (commands.length) {
        message.push(`Commands blocked in guild:${tag}`);
        message.push(commands.join(', '));
        message.push(tag);
        commands = [];
    }

    bl = SkittyBot.channel_command_blacklist.get(msg.channel.id);
    if (Array.isArray(bl)) bl.forEach(t => commands.push(t));
    if (commands.length) {
        message.push(`Commands blocked in this channel:${tag}`);
        message.push(commands.join(', '));
        message.push(tag);
    }

    bl = SkittyBot.guild_module_blacklist.get(msg.guild.id);
    if (Array.isArray(bl)) bl.forEach(t => modules.push(t));
    if (modules.length) {
        message.push(`Modules blocked in this guild:${tag}`);
        message.push(modules.join(', '));
        message.push(tag);
        modules = [];
    }

    bl = SkittyBot.channel_module_blacklist.get(msg.channel.id);
    if (Array.isArray(bl)) bl.forEach(t => modules.push(t));
    if (modules.length) {
        message.push(`Modules blocked in this channel:${tag}`);
        message.push(modules.join(', '));
        message.push(tag);
    }

    if (!message.length) {
        msg.reply(`Nothing to show.`);
        return;
    }

    msg.channel.send(message.join('\n'));
}

async function bot_register_custom_alias(msg, args) {
    let command = param('(command|cmd)', msg.content);
    let alias = param('alias', msg.content);
    let def_arg = param('(arguments?|args?)', msg.content, '');
    if (!command || !alias) {
        command = args.shift();
        alias = args.shift();
        def_arg = args.join(' ');
    }
    if (!command || !alias) {
        msg.reply([
            `You need at least two parameters for this command two work, a command name and new alias for that command. They can be provided in different ways.`,
            `Examples:`,
            '```',
            `createalias say talk`,
            `createalias --command say --alias talk`,
            `createalias say talk this is a test`,
            `createalias --command say --alias talk --args testing 123`,
            '```'
        ].join('\n'));
        return;
    }

    command = command.toLowerCase();
    alias = alias.toLowerCase();

    if (!/[a-z0-9-]{1,32}/i.test(alias)) {
        msg.reply([
            `Sorry, but \`${alias}\` does not meet the required criteria for defining a new command alias.`,
            `Make sure your new alias consists exclusively of alphanumeric characters (hyphen allowed) and is under 32 characters in length.`,
            `This means no white-spaces in the alias.`
        ].join('\n'));
        return;
    }

    let oCMD = SkittyBot.getCommand(command);
    if (typeof oCMD !== 'object') {
        msg.reply([
            `Command \`${command}\` is neither defined as a command or command alias in my registry.`,
            `Please check your input and make sure you are using a correct command name or the commands alias and try again.`
        ].join('\n'));
        return;
    }

    command = oCMD.command;

    let aliases = SkittyBot.custom_commands.get(msg.guild.id);
    if (typeof aliases !== 'object') {
        aliases = new Map();
        SkittyBot.custom_commands.set(msg.guild.id, aliases);
    }
    // check if we already have that in our DB
    let results = await SkittyBot.system_database.get('SELECT * FROM custom_commands WHERE guild_id = ? AND alias = ?', msg.guild.id, alias).catch(console.error);
    if (results && results.guild_id === msg.guild.id) {
        // get rid of it
        await SkittyBot.system_database.run('DELETE FROM custom_commands WHERE guild_id = ? AND alias = ?', msg.guild.id, alias).catch(console.error);
        if (aliases.has(alias)) aliases.delete(alias);
    }
    SkittyBot.system_database.run('INSERT INTO custom_commands (guild_id, alias, command, args) VALUES (?, ?, ?, ?);', msg.guild.id, alias, command, def_arg)
        .then(() => {
            aliases.set(alias, { command: command, args: def_arg });
            let addendum = def_arg.length ? ` with default arguments set to \`${def_arg}\`` : `!`;
            msg.channel.send(`Ok, \`${alias}\` is now an alias for \`${command}\`${addendum}`);
        })
        .catch(err => {
            // strange error, don't even register it with the Map object
            SkittyBot.logger.error(err);
            msg.channel.send(`Huh... Looks like something went wring adding this to my registry. Please report the issue with my developer!`);
        });
}

function bot_unregister_custom_alias(msg, args) {
    let alias = param('alias', msg.content);
    if (!alias) alias = args.pop();
    if (!alias) {
        msg.reply([
            `You need an alias for this command two work, aka the alias you want to delete.`,
            'Example: `unregisteralias talk`'
        ].join('\n'));
        return;
    }
    let aliases = SkittyBot.custom_commands.get(msg.guild.id);
    if (typeof aliases === 'object') aliases.delete(alias);
    SkittyBot.system_database.run('DELETE FROM custom_commands WHERE guild_id = ? AND alias = ?;', msg.guild.id, alias)
        .then(res => {
            if (res.changes) {
                msg.channel.send(`Ok.`);
            } else {
                msg.reply(`There didn't seem to be anything here for "${alias}".`);
            }
        })
        .catch(err => {
            console.error(err);
            msg.channel.send(`Oh no! Something went wrong. There's more information in my console if you'd have a look.`);
        });
}

function bot_rename_user(msg, args) {
    let user = args.shift();
    let user_id = /(\d{17,19})/.exec(user);
    if (Array.isArray(user_id)) {
        user_id = user_id[0];
    } else {
        args.unshift(user);
        user_id = msg.author.id;
    }
    if (user_id !== msg.author.id) {
        if (!msg.member.hasPermission('MANAGE_NICKNAMES')) {
            msg.channel.send(`You can only change your own nickname unless you have the "manage nicknames" permission...`);
            return;
        }
    }
    let mine = msg.guild.me.roles.highest.position;
    let new_name = args.join(' ');
    if (user_id) {
        let member = msg.guild.members.get(user_id);
        if (member) {
            let nick = member.nickname || '';
            let users = member.roles.highest.position;
            if (users >= mine && user_id !== SkittyBot.client.user.id) {
                msg.reply(`I can't nickname people with roles higher than mine.`);
                return;
            }
            // nick doesn't want to be renamed with the bot
            if (member.id === '270286142702747648') {
                msg.reply(`Unable to comply.`);
                return;
            }
            member.setNickname(new_name)
                .then(() => {
                    msg.react('👌').catch(e => {
                        if (e && e.code === 90001) {
                            member.setNickname(nick);
                        }
                    });
                })
                .catch(e => {
                    msg.channel.send(`I wasn't able to do that...`);
                    console.log(e);
                });
        } else {
            msg.channel.send(`User not found.`).then(m => m.delete(3000));
        }
    }
}

function bot_undelete_user_messages(msg, args) {
    if (!m_cap) {
        msg.reply(`Sorry, but it would appear that the message capture class was not initiated.`);
        return;
    }
    let user = msg.mentions.users.first();
    let user_id;
    if (!user) {
        user_id = /\d{10,}/.exec(args.shift());
        if (Array.isArray(user_id)) user = { id: user_id[0] };
        if (!user) {
            msg.channel.send('Could not get a user ID from your command.');
            return;
        }
    }

    let cache = m_cap.deleted.get(user.id);
    if (!cache || !Array.isArray(cache)) {
        msg.channel.send('Nothing found.');
        return;
    }

    function present(collection, index) {

        let message = collection[index];

        let uid = /\d{10,}/.exec(message.content);
        if (Array.isArray(uid)) {
            uid.forEach(mention => {
                let user_object = SkittyBot.client.users.get(mention);
                if (user_object !== undefined) message.content = message.content.replace(String(mention), `${user_object.username}#${user_object.discriminator}`);
            });
        }

        let embed = new MessageEmbed()
            .setAuthor(`${message.author.username}#${message.author.discriminator}`, message.author.displayAvatarURL({ size: 128 }))
            .setTitle(`Deleted message ${index + 1} of ${collection.length} in ${message.guild.name}#${message.channel.name}`)
            .setDescription(message.content.replace(new RegExp('(\\*|_|`)', 'g'), '\\$1'));
        let attachment_key = message.attachments.keys().next().value;
        if (attachment_key) {
            let attachment = message.attachments.get(attachment_key);
            if (attachment && attachment.url) embed.setImage(attachment.url);
        }
        return embed;
    }

    let page = 0;

    msg.channel.send(present(cache, page)).then(message => {

        if (cache.length === 1) return;

        message.react('◀').then(() => {
            message.react('▶').then(() => {
                message.react('❌');
            });
        });

        SkittyBot.onReaction(message, reaction => {
            if (reaction.user_id !== msg.author.id) return;
            let new_page = page;
            switch (reaction.name) {
                case '◀':
                    if (page > 0)--page;
                    break;
                case '▶':
                    if (cache.length - 1 > page)++page;
                    break;
                case '❌':
                    reaction.stop();
            }
            if (new_page !== page) message.edit(present(cache, page)).catch(console.error);
        })
            .end(() => {
                message.reactions.removeAll();
            });

    });

}

async function bot_prune_messages(msg, args) {

    let MAX_AGE = 1000 * 60 * 60 * 24 * 14; // 14 days in miliseconds, this is the amount of time passed that a message can no longer be deleted by a bot
    let MAX_PRUNE = 5000;
    let number;
    let mentions = msg.mentions;
    let channel = msg.channel;
    let members = msg.guild.members;
    let discriminate = mentions.users.size > 0;
    let targets = [];

    let from, to, ranged = false;

    let setProgress = async (text, r) => {
        await progress.edit(text).catch(console.error);
        setTimeout(resolve => {
            resolve();
        }, 5000, r);
    };

    let sleep = ms => new Promise(r => {
        setTimeout(() => r(), ms);
    });

    let old = m => {
        if (typeof m !== 'object') return false;
        return (+new Date() - m.createdTimestamp) > MAX_AGE;
    };

    if (discriminate) targets = mentions.users.map(m => m.id);

    args.forEach((arg, index) => {
        if (Number.isSafeInteger(parseInt(arg))) {
            number = parseInt(arg);
        } else if (members.has(arg)) {
            discriminate = true;
            targets.push(arg);
        } else if (isNaN(arg) && typeof arg === 'string') {
            switch (arg.toLowerCase()) {
                case 'from': from = args[++index]; break;
                case 'to': to = args[++index]; break;
            }
        }
    });

    if ((from && !to) || (!from && to) || (from && !/^\d{7,}$/.test(from)) || (to && !/^\d{7,}$/.test(to))) {
        channel.send([
            `The from and to argument was not correctly set. Example: \`from messageID to messageID\` where messageID is an ID of two different messages.`,
            `When correctly set, I will delete all messages between the \`from\` and \`to\` messages.`,
            `You can also target specific user messages between the provided IDs by mentioning them or adding their user IDs to the command.`,
            `Example: \`prune from 123456789 to 987654321 @MentionedUser1 @MentionedUser2\`.`
        ].join('\n'));
        return;
    }

    if (from && to) {
        if (from === to) {
            msg.channel.send(`The \`from\` and \`to\` parameters point to the same message ID. Don't use mass deletion commands for single message pruning!`);
            return;
        }
        ranged = true;
        let msgFrom = await channel.messages.fetch(from).catch(console.error);
        if (!msgFrom) {
            channel.send(`Unable to find \`from\` message ID: ${from} in this channel.`);
            return;
        } else if (old(msgFrom)) {
            channel.send(`The \`from\` message is too old to be deleted. Cannot delete messages older than two weeks.`);
            return;
        }
        let msgTo = await channel.messages.fetch(to).catch(console.error);
        if (!msgTo) {
            channel.send(`Unable to find \`to\` message ID: ${to} in this channel.`);
            return;
        } else if (old(msgTo)) {
            channel.send(`The \`from\` message is too old to be deleted. Cannot delete messages older than two weeks.`);
            return;
        }
        if (msgFrom.createdTimestamp < msgTo.createdTimestamp) {
            // switch IDs because end users are <insert derogatory remark>
            let switcharoo = from;
            from = to;
            to = switcharoo;
        }
        msgFrom.delete();
    }

    if (!ranged) {
        if (!number) {
            msg.reply(`run \`${SkittyBot.prefix(msg)}help prune\` to learn how to use this command.`);
            return;
        }

        if (number > MAX_PRUNE) {
            msg.reply(`sorry, but I can only delete a maximum of ${MAX_PRUNE} messages in one call.`);
            return;
        }
    } else if (ranged && !number) {
        number = MAX_PRUNE / 2;
    }

    let chunks = [];
    while (number) {
        if (number > 100) {
            chunks.push(100);
            number -= 100;
        } else {
            chunks.push(number);
            number = 0;
        }
    }

    let options = {
        before: ranged ? from : msg.id
    };

    let totalHandled = 0;
    let usersHandled = [];
    let stopService = false;
    let lastError;
    let TooOldToContinue = false;

    let progress = await channel.send(`Initiated ${ranged ? 'ranged ' : ''}prune routine with ${chunks.length} chunk${chunks.length > 1 ? 's' : ''}...`);

    await sleep(3000);

    switch (discriminate) {
        case true: {

            await chunks.reduce((p, amount, index) => p.then(async () => {
                if (stopService) return;
                await new Promise(r => {
                    options.limit = amount;
                    channel.messages.fetch(options)
                        .then(messageCache => {

                            if (!messageCache.size) {
                                setProgress(`Chunk ${index + 1}/${chunks.length}: ${totalHandled} message${totalHandled > 1 ? 's' : ''} from ${usersHandled.length} user${usersHandled.length > 1 ? 's' : ''} removed.`, r);
                                stopService = true;
                                return;
                            }

                            messageCache = messageCache.array();

                            messageCache = messageCache.filter(m => !old(m));
                            if (!messageCache.length) {
                                TooOldToContinue = true;
                                stopService = true;
                                r();
                                return;
                            }

                            options.before = messageCache[messageCache.length - 1].id;
                            messageCache = messageCache.filter(m => {
                                if (stopService) return false;
                                if (m.id === to) {
                                    stopService = true;
                                    return true;
                                }
                                let bool = targets.includes(m.author.id) && m.deletable;
                                if (bool && !usersHandled.includes(m.author.id)) usersHandled.push(m.author.id);
                                return bool;
                            });

                            if (!messageCache.length) {
                                r();
                                return;
                            }

                            if ((ranged && number && number > totalHandled) && (number < totalHandled + messageCache.length)) {
                                // we need to stop and reduce the amount of messages that are going to be deleted to match
                                messageCache = messageCache.slice(0, number - totalHandled);
                                stopService = true;
                            }

                            msg.channel.bulkDelete(messageCache)
                                .then(() => {
                                    // looks like API isn't returning the deleted messages anymore
                                    // totalHandled += messages.size;
                                    totalHandled += messageCache.length;
                                    setProgress(`Chunk ${index + 1}/${chunks.length}: ${totalHandled} message${totalHandled > 1 ? 's' : ''} from ${usersHandled.length} user${usersHandled.length > 1 ? 's' : ''} removed.`, r);
                                })
                                .catch(err => {
                                    console.error(err);
                                    setProgress(`Chunk ${index + 1}/${chunks.length}: ERROR: ${err.message}`, r);
                                    lastError = err.message;
                                    stopService = true;
                                });

                        })
                        .catch(err => {
                            console.error(err);
                            setProgress(`Chunk ${index + 1}/${chunks.length}: ERROR: ${err.message}`, r);
                            lastError = err.message;
                            stopService = true;
                        });
                });
            }), Promise.resolve());

            break;
        }
        case false: {

            await chunks.reduce((p, amount, index) => p.then(async () => {
                if (stopService) return;
                await new Promise(r => {
                    options.limit = amount;
                    channel.messages.fetch(options)
                        .then(messageCache => {

                            if (!messageCache.size) {
                                setProgress(`Chunk ${index + 1}/${chunks.length}: ${totalHandled} message${totalHandled > 1 ? 's' : ''} from ${usersHandled.length} user${usersHandled.length > 1 ? 's' : ''} removed.`, r);
                                stopService = true;
                                return;
                            }

                            messageCache = messageCache.array();

                            messageCache = messageCache.filter(m => !old(m));
                            if (!messageCache.length) {
                                TooOldToContinue = true;
                                stopService = true;
                                return;
                            }

                            options.before = messageCache[messageCache.length - 1].id;
                            messageCache = messageCache.filter(m => {
                                if (stopService) return false;
                                if (m.id === to) {
                                    stopService = true;
                                    return true;
                                }
                                let bool = m.deletable;
                                if (bool && !usersHandled.includes(m.author.id)) usersHandled.push(m.author.id);
                                return bool;
                            });

                            if (!messageCache.length) {
                                r();
                                return;
                            }

                            if ((ranged && number && number > totalHandled) && (number < totalHandled + messageCache.length)) {
                                // we need to stop and reduce the amount of messages that are going to be deleted to match
                                messageCache = messageCache.slice(0, number - totalHandled);
                                stopService = true;
                            }

                            msg.channel.bulkDelete(messageCache)
                                .then(() => {
                                    // looks like API isn't returning the deleted messages anymore
                                    // totalHandled += messages.size;
                                    totalHandled += messageCache.length;
                                    setProgress(`Chunk ${index + 1}/${chunks.length}: ${totalHandled} message${totalHandled > 1 ? 's' : ''} from ${usersHandled.length} user${usersHandled.length > 1 ? 's' : ''} removed.`, r);
                                })
                                .catch(err => {
                                    console.error(err);
                                    setProgress(`Chunk ${index + 1}/${chunks.length}: ERROR: ${err.message}`, r);
                                    lastError = err.message;
                                    stopService = true;
                                });

                        })
                        .catch(err => {
                            console.error(err);
                            setProgress(`Chunk ${index + 1}/${chunks.length}: ERROR: ${err.message}`, r);
                            lastError = err.message;
                            stopService = true;
                        });
                });
            }), Promise.resolve());
        }
    }

    let appendix = TooOldToContinue ? `\nThe messages are too old to delete.` : '';
    if (lastError) appendix += `\nLast Error: ${lastError}`;

    progress.edit(`Prune routine finish with ${totalHandled} message${totalHandled > 1 ? 's' : ''} from ${usersHandled.length} user${usersHandled.length > 1 ? 's' : ''} removed.${appendix}`);

    setTimeout(() => {
        msg.delete();
        progress.delete();
    }, 3000);

}

function bot_permissions_view(msg) {
    let user = msg.mentions.users.first();
    let avatarURL;
    if (!user) {
        user = msg.guild.members.get(msg.author.id);
        avatarURL = msg.author.displayAvatarURL();
    } else {
        avatarURL = user.displayAvatarURL();
        user = msg.guild.members.get(user.id);
    }
    let perms = user.permissionsIn(msg.channel.id);
    let aPerms = perms.toArray();
    let highestPoint = 0;
    aPerms.forEach(perm => {
        perm = `${perm}:`;
        if (perm.length > highestPoint) highestPoint = perm.length;
    });
    aPerms.forEach((perm, index) => {
        aPerms[index] = `${perm.padEnd(highestPoint, ' ')}:${permissions[perm]}`;
    });
    aPerms.push('```');
    aPerms.unshift('```');
    let embed = new MessageEmbed();
    embed.setAuthor(user.displayName, avatarURL)
        .setDescription(aPerms);
    msg.channel.send(embed);
}

async function bot_set_prefix(msg, args) {
    let del = SkittyBot.utils.param('delete', args.join(' '), false);
    if (del) {
        SkittyBot.system_database.run('DELETE FROM prefixes WHERE guild_id = ?;', msg.guild.id)
            .then(res => {
                if (res.changes) {
                    SkittyBot.guild_prefix.delete(msg.guild.id);
                    msg.reply(`your guild prefix is no more.`);
                } else {
                    msg.reply(`There was nothing to delete!`);
                }
            })
            .catch(console.error);
        return;
    }
    if (!args[0]) {
        let guild_prefix = SkittyBot.guild_prefix.get(msg.guild.id);
        if (guild_prefix) {
            msg.channel.send(`The prefix in this server is \`${guild_prefix}\`.`);
        } else {
            msg.channel.send([
                `I haven't been given a command prefix to use here.`,
                `You can give me a prefix with the command \`${SkittyBot.config.bot_prefix} prefix >\`.`,
                `Just copy and paste that if you're a guild manager and I'll begin using that prefix for my commands!`
            ].join('\n'));
        }
        return;
    }
    if (args.length > 1) {
        msg.channel.send(`A prefix cannot have spaces. It can be a word or combination of characters. Make sure to chose something that's easy to remember or use!`);
        return;
    }
    if (!SkittyBot.guild_prefix.has(msg.guild.id)) {
        await SkittyBot.system_database.run('INSERT INTO prefixes (guild_id, prefix) VALUES (?, ?);', msg.guild.id, args[0]);
    } else {
        await SkittyBot.system_database.run('UPDATE prefixes SET prefix = ? WHERE guild_id = ?;', args[0], msg.guild.id);
    }
    SkittyBot.guild_prefix.set(msg.guild.id, args[0]);
    msg.channel.send(`OK,\`${args[0]}\` is my new prefix in this guild.`);
}

async function bot_set_personal_prefix(msg, args) {
    let del = SkittyBot.utils.param('delete', args.join(' '), false);
    if (del) {
        SkittyBot.system_database.run('DELETE FROM personal_prefixes WHERE user_id = ?;', msg.author.id)
            .then(res => {
                if (res.changes) {
                    SkittyBot.user_prefix.delete(msg.author.id);
                    msg.reply(`your personal prefix is no more.`);
                } else {
                    msg.reply(`There was nothing to delete!`);
                }
            })
            .catch(console.error);
        return;
    }
    if (!args[0]) {
        let user_prefix = SkittyBot.user_prefix.get(msg.author.id);
        if (user_prefix) {
            msg.reply(`the personal prefix you chose to use with me was \`${user_prefix}\`. To stop using a personal prefix, use the command \`${SkittyBot.prefix(msg)}my-prefix --delete\``);
        } else {
            msg.channel.send([
                `You can give me a personal prefix with the command \`${SkittyBot.prefix(msg)}prefix some-prefix\`, where "some-prefix" is whatever you choose.`,
                `Once you set a personal prefix, I will use that prefix for you wherever we are, even DMs! A personal prefix does not override a guild prefix, you will be able to invoke commands with either.`
            ].join('\n'));
        }
        return;
    }
    if (args.length > 1) {
        msg.channel.send(`A prefix cannot have spaces. It can be a word or combination of characters. Make sure to chose something that's easy to remember or use!`);
        return;
    }
    if (!SkittyBot.user_prefix.has(msg.author.id)) {
        await SkittyBot.system_database.run('INSERT INTO personal_prefixes (user_id, prefix) VALUES (?, ?);', msg.author.id, args[0]);
    } else {
        await SkittyBot.system_database.run('UPDATE personal_prefixes SET prefix = ? WHERE user_id = ?;', args[0], msg.author.id);
    }
    SkittyBot.user_prefix.set(msg.author.id, args[0]);
    msg.channel.send(`Ok, I'll now accept \`${args[0]}\` as a command prefix for messages from you.`);
}

const configuration = {
    noDisable: true,
    onload: () => {
        m_cap = new message_capture();
    },
    onunload: () => {
        m_cap.destroy();
    },
    commands: [
        {
            command: 'prefix',
            aliases: [
                'sbprefix',
                'setprefix',
                'sbp',
                'fbp'
            ],
            example: [
                'sbprefix .',
                'prefix something',
                'sbp sb',
                'prefix !'
            ],
            description: [
                'This will set my prefix in the guild.',
                'If there is a prefix collision with another bot then consider using one of my aliases for this command.',
                `To delete your guilds prefix for any reason, run the command with --delete`
            ],
            guildOnly: true,
            member: [
                'MANAGE_GUILD'
            ],
            process: bot_set_prefix
        },
        {
            command: 'set-personal-prefix',
            aliases: [
                'my-personal-prefix',
                'my-prefix',
                'mpp',
                'spp'
            ],
            example: [
                'mpp .',
                'mpp something',
                'mpp sb',
                'mpp !'
            ],
            description: [
                'Just like a guild prefix, but specifically for you.',
                `Setting up a personal prefix is helpful if you intend to run commands in the bots DMs or just don't want to memorize a bunch of different prefixes in different places.`,
                `To delete your personal prefix, run the command with --delete`
            ],
            process: bot_set_personal_prefix
        },
        {
            command: 'createalias',
            description: [
                `Creates a custom alias for any command with an option to use default parameters when using the command alias and none were provided.`,
                `Options:`,
                `   --command: the name of the command to create an alias for`,
                `   --alias: the new name you want to use for this command in your server`,
                `   --args: [optional] default arguments to pass to the alias command when invoking, providing arguments when using the command overrides these default arguments`
            ],
            example: [
                'createalias --command say --alias talk --args is this talking?'
            ],
            aliases: [
                'registeralias'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            process: bot_register_custom_alias,
            guildOnly: true
        },
        {
            command: 'deletealias',
            description: [
                `Deletes a custom alias that someone previously created.`
            ],
            example: [
                'deletealias talk'
            ],
            aliases: [
                'unregisteralias'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            process: bot_unregister_custom_alias,
            guildOnly: true
        },
        {
            command: 'say',
            example: 'say testing testing 1 2 3...',
            description: `Good for testing whether or not other bots ignore bots.`,
            process: (msg, args) => {
                if (!args[0]) {
                    msg.reply(`Say what?`);
                    return;
                }
                let m = args.join(' ');
                msg.channel.send(m.replace(/@everyone/ig, '@everypony').replace(/@here/ig, 'here'));
            },
            guildOnly: true
        },
        {
            command: 'saydelete',
            aliases: [
                `deletesay`,
                `saydel`,
                `delsay`
            ],
            example: 'delsay testing testing 1 2 3...',
            description: `This command is like the regular say command except it will delete your command message immediately.`,
            member: [
                'MANAGE_MESSAGES'
            ],
            client: [
                'MANAGE_MESSAGES'
            ],
            process: (msg, args) => {
                if (!args[0]) {
                    msg.reply(`Say what?`);
                    return;
                }
                let p = args.join(' ');
                msg.delete();
                msg.channel.send(p);
            },
            guildOnly: true
        },
        {
            command: 'blockuser',
            description: [
                `Globally block a user from the bot.`
            ],
            example: [
                'blockuser @mention'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            owner: true,
            ring: 3,
            private: true,
            process: (msg, args) => {
                blacklist_handler.block_user(msg, args, false, true, true);
            }
        },
        {
            command: 'unblockuser',
            description: [
                `Unblocks a user who was previously globally blocked from the bot.`
            ],
            example: [
                'unblockuser @mention'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            owner: true,
            ring: 3,
            private: true,
            process: (msg, args) => {
                blacklist_handler.block_user(msg, args, false, false, true);
            }
        },
        {
            command: 'channelblockuser',
            aliases: [
                'cbu'
            ],
            description: [
                `Blocks a user from using the bot in a guild channel.`
            ],
            example: [
                'channelblockuser @mention'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            process: (msg, args) => {
                blacklist_handler.block_user(msg, args, true, true);
            }
        },
        {
            command: 'channelunblockuser',
            aliases: [
                'cuu'
            ],
            description: [
                `If you previously blocked a user by making the bot ignore them in a channel, this command will undo that action.`
            ],
            example: [
                'channelunblockuser @user'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            process: (msg, args) => {
                blacklist_handler.block_user(msg, args, true, false);
            }
        },
        {
            command: 'guildblockuser',
            aliases: [
                'gbu'
            ],
            description: [
                `Blocks a user from using the bot in your guild.`
            ],
            example: [
                'guildblockuser @user'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            process: (msg, args) => {
                blacklist_handler.block_user(msg, args, false, true);
            }
        },
        {
            command: 'guildunblockuser',
            aliases: [
                'guu'
            ],
            description: [
                `If you previously blocked a user by making the bot ignore them in your guild, this command will undo that action.`
            ],
            example: [
                'guildunblockuser @user'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            process: (msg, args) => {
                blacklist_handler.block_user(msg, args, false, false);
            }
        },
        {
            command: 'devdisablecommand',
            aliases: [
                'ddc'
            ],
            description: [
                `Globally disable a command.`
            ],
            example: [
                'devdisablecommand owo'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            owner: true,
            ring: 2,
            private: true,
            process: (msg, args) => {
                blacklist_handler.block_command(msg, args, false, true, true);
            }
        },
        {
            command: 'devenablecommand',
            aliases: [
                'dec'
            ],
            description: [
                `Globally enable a command.`
            ],
            example: [
                'devenablecommand owo'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            owner: true,
            ring: 2,
            private: true,
            process: (msg, args) => {
                blacklist_handler.block_command(msg, args, false, false, true);
            }
        },
        {
            command: 'devdisablemodule',
            aliases: [
                'ddm'
            ],
            description: [
                `Globally disable a module.`
            ],
            example: [
                'devdisablemodule twitter bot'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            owner: true,
            ring: 2,
            private: true,
            process: (msg, args) => {
                blacklist_handler.block_module(msg, args, false, true, true);
            }
        },
        {
            command: 'devenablemodule',
            aliases: [
                'dem'
            ],
            description: [
                `Globally enable a module.`
            ],
            example: [
                'devenablemodule twitter bot'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            owner: true,
            ring: 2,
            private: true,
            process: (msg, args) => {
                blacklist_handler.block_module(msg, args, false, false, true);
            }
        },
        {
            command: 'channeldisablecommand',
            aliases: [
                'cdc'
            ],
            description: [
                `Disables use of a command in a channel using its name.`
            ],
            example: [
                'channeldisablecommand owo'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            process: (msg, args) => {
                blacklist_handler.block_command(msg, args, true, true);
            }
        },
        {
            command: 'channelenablecommand',
            aliases: [
                'cec'
            ],
            description: [
                `Enables use of a command in a channel when previously disabled using its name.`
            ],
            example: [
                'channelenablecommand owo'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            process: (msg, args) => {
                blacklist_handler.block_command(msg, args, true, false);
            }
        },
        {
            command: 'guilddisablecommand',
            aliases: [
                'gdc'
            ],
            description: [
                `Disables use of a command in a guild using its name.`
            ],
            example: [
                'guilddisablecommand owo'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            process: (msg, args) => {
                blacklist_handler.block_command(msg, args, false, true);
            }
        },
        {
            command: 'guildenablecommand',
            aliases: [
                'gec'
            ],
            description: [
                `Enables use of a command in a guild when previously disabled using its name..`
            ],
            example: [
                'guildenablecommand owo'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            process: (msg, args) => {
                blacklist_handler.block_command(msg, args, false, false);
            }
        },
        {
            command: 'channeldisablemodule',
            aliases: [
                'cdm'
            ],
            description: [
                `Disables use of a module in a channel using its name. Get a list of module names with the modules command.`
            ],
            example: [
                'channeldisablemodule twitter bot'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            process: (msg, args) => {
                blacklist_handler.block_module(msg, args, true, true);
            }
        },
        {
            command: 'channelenablemodule',
            aliases: [
                'cem'
            ],
            description: [
                `Enables use of a module in a channel when previously disabled using its name. Get a list of module names with the modules command.`
            ],
            example: [
                'channelenablemodule twitter bot'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            process: (msg, args) => {
                blacklist_handler.block_module(msg, args, true, false);
            }
        },
        {
            command: 'guilddisablemodule',
            aliases: [
                'gdm'
            ],
            description: [
                `Disables use of a module in a guild using its name. Get a list of module names with the modules command.`
            ],
            example: [
                'guilddisablemodule twitter bot'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            process: (msg, args) => {
                blacklist_handler.block_module(msg, args, false, true);
            }
        },
        {
            command: 'guildenablemodule',
            aliases: [
                'gem'
            ],
            description: [
                `Enables use of a module in a guild when previously disabled using its name. Get a list of module names with the modules command.`
            ],
            example: [
                'guildenablemodule twitter bot'
            ],
            member: [
                'MANAGE_GUILD'
            ],
            guildOnly: true,
            noDisable: true,
            process: (msg, args) => {
                blacklist_handler.block_module(msg, args, false, false);
            }
        },
        {
            command: 'showdisabled',
            description: [
                `Lists all blocked commands and modules in the or channel.`
            ],
            aliases: [
                'disabled'
            ],
            guildOnly: true,
            noDisable: true,
            process: bot_list_blocked_items
        },
        {
            command: 'nick',
            example: [
                'nick @mention TheirNewName',
                'nick MyNewName'
            ],
            description: [
                'Changes a users nickname or your own if no mention was provided.',
                'If no new name is provided, it will remove a nickname if they have one.'
            ],
            client: [
                'CHANGE_NICKNAME',
                'MANAGE_NICKNAMES'
            ],
            member: [
                'MANAGE_NICKNAMES'
            ],
            guildOnly: true,
            process: bot_rename_user
        },
        {
            command: 'undelete',
            example: [
                'undelete @user',
                'undelete 164171368517206016'
            ],
            description: [
                `Lists deleted message for a @mentioned user unless the message was deleted in a bulk delete action performed by a bot.`,
                `In some cases it doesn't save the message when a server owner performed the delete for some reason.`,
                `The deleted message cache is limited to 25 per users.`,
                `If more than 25 messages were deleted by a user, the old messages are bumped off the cache.`,
                `Messages are not preserved and are lost on bot reboot.`
            ],
            client: [
                'MANAGE_MESSAGES'
            ],
            member: [
                'MANAGE_MESSAGES'
            ],
            guildOnly: true,
            aliases: ['udm', 'un'],
            process: bot_undelete_user_messages
        },
        {
            command: 'prune',
            example: [
                'prune 10',
                'prune 1000 @user',
                'prune 2000 @user1 @user2 @user3',
                'prune user_id1 user_id2 @user3 5000',
                'prune from MessageID to MessageID',
                'prune user_id1 user_id2 @user3 from MessageID to MessageID'
            ],
            description: [
                `This command deletes messages in a channel.`,
                `You can delete up to a maximum of 5k messages in one call.`,
                `You can mention users you want messages deleted from and or provide their IDs, mentions and IDs can be mixed.`,
                `The amount of messages to delete can be a range between 1 and 5000, range can be anywhere in the command, it does not matter.`,
                `The "from ID to ID" argument tells the bot to delete messages between the message IDs you have provided. The from and to message will also be deleted.`,
                `You can choose to target user messages in a from to range by mentioning them.`,
                '',
                [
                    `Note: When pruning messages that target user(s), the number of messages you tell it to delete does not translate to a number of messages that the bot will delete,`,
                    `but rather the number of messages that the bot will search through in order to find messages to delete!`
                ].join(' ')
            ],
            client: [
                'MANAGE_MESSAGES'
            ],
            member: [
                'MANAGE_MESSAGES'
            ],
            aliases: [
                'bulkdelete',
                'delete',
                'del'
            ],
            guildOnly: true,
            process: bot_prune_messages
        },
        {
            command: 'permissions',
            example: [
                'permissions',
                `perms @mention`
            ],
            description: [
                'Provides permissions information on the bot in the channel used when no user was mentioned.',
                `If a user is mentioned, permissions info for that user is listed instead.`
            ],
            aliases: [
                'perms'
            ],
            guildOnly: true,
            process: bot_permissions_view
        }
    ]
};

SkittyBot.registerPlugin(configuration);
